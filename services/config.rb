coreo_agent_selector_rule 'check-mysqld' do
    action :define
    timeout 30
    control 'check-mysqld' do
        describe command('mysqld') do
            it { should exist }
        end
    end
end

coreo_agent_audit_profile 'mysql-baseline' do
    action :define
    selectors ['check-mysqld']
    profile 'https://s3.amazonaws.com/coreo-agent/inspec-profiles/mysql-baseline-master-backup.zip'
    timeout 120
end

coreo_agent_rule_runner 'audit-mysql-profiles' do
    action :run
    profiles ${AUDIT_MYSQL_PROFILES_ALERT_LIST}
    filter(${FILTERED_OBJECTS}) if ${FILTERED_OBJECTS}
end
  
