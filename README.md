Audit MySQL Applicaiton
============================
This stack will perform MySQL application baseline

## Description
This composite will audit MySQL application for MySQL Baseline

* ![DevSec MySQL Baseline](https://github.com/dev-sec/mysql-baseline "Inspec profile github link")

## Hierarchy



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

### `AUDIT_MYSQL_PROFILES_ALERT_LIST`:
  * description: 
  * default: mysql-baseline


## Optional variables with no default

### `FILTERED_OBJECTS`:
  * description: JSON object of string or regex of aws objects to include or exclude and tag in audit

## Tags
1. Audit
1. Best Practices
1. CIS

## Categories


## Diagram


## Icon


